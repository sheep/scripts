#!/bin/bash

echo -e "Timezone listing"

for zone in 'America/New_York' 'Asia/Hong_Kong' 'America/Los_Angeles' 'Europe/London'
do
    out=$(echo $zone | sed 's/.*\///;s/_/ /')
    real_time=$(TZ=$zone date "+$out (%:z) \033[32;1m%R\033[0m %F")
    echo -e $real_time
done

