#!/bin/bash

# Newsboat manager for cron and bars
case $1 in
    update|up)
        # Wait for no newsboat, then reload.
        pidwait -x "newsboat"
        newsboat -x reload
        ;;
    fetch|fe)
        # Check if newsboat is running. If so, return count.
        # Else, return padding.
        if [ -z $(pgrep -x "newsboat") ]; then
            newsboat -x print-unread | sed 's/ .*/ articles/;s/^/ /'
        else
            echo " ..."
        fi
        ;;
esac
