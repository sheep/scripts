#!/usr/bin/luajit

local lfs = require("lfs")

local lua_dir = arg[0]:match("@?(.*/)")

-- Clear
print("\27[H\27[2J\27[3J")

-- Print out flower from text file
local flower = io.open(lua_dir .. "flower.txt", "r")
if flower then
   print(flower:read("*a"))
   flower:close()
else
   print("Error: Missing ASCII flower")
   os.exit(-1)
end

local get_file = function(location, permissions, split_str)
   local table = {}
   local got_file = io.open(location, permissions)
   if got_file then
      for line in got_file:lines() do
         for line_id, line_val in line:gmatch(split_str) do
            table[line_id] = line_val
         end
      end
      got_file:close()
   end
   return table
end

local get_line = function(location)
   local item = io.open(location, 'r')
   if item then
      local str = item:read('*l')
      item:close()
      return str
   end
end

-- Get battery stuff
local battery = ""
local batteries = ""
for filename in lfs.dir("/sys/class/power_supply") do
   if filename ~= "." and filename ~= ".." and filename ~= "AC" then
      battery = get_line("/sys/class/power_supply/" .. filename .. "/capacity")
      batteries = string.format("[ %s %s%% ] ", filename, battery) .. batteries
   end
end

-- Get wifi link quality
local w_initial = {}
local wifi_string = ""
local wifi = io.open('/proc/net/wireless', 'r')
if wifi then
   for line in wifi:lines() do
      table.insert(w_initial, line)
   end

   -- what the fuck is lua regex.
   local wifi_regex = "%s(.*):.*%s(%d+)%."

   -- Assemble wifi into printable set of strings.
   for item = 1, #w_initial - 2 do
      for w_if, w_qual in w_initial[item + 2]:gmatch(wifi_regex) do
         wifi_string = wifi_string .. string.format("[ %s %s%% ] ", w_if, w_qual)
      end
   end
   wifi:close()
end

-- Get individual elements
local model = get_line('/sys/devices/virtual/dmi/id/product_name')
local kernel = get_line('/proc/sys/kernel/osrelease')
local shell = os.getenv("SHELL"):gsub(".*/", "")
local os_rel = get_file("/etc/os-release", "r", "(.*)=\"(.*)\"")
local uptime = get_line("/proc/uptime"):gsub("%..*", "")
local up_d = math.floor(uptime / (3600 * 24))
local up_h = math.floor((uptime % (3600 * 24)) / 3600)
local up_m = math.floor(((uptime % (3600 * 24)) % 3600) / 60)

local upstring = ""
if up_d > 0 then
   upstring = string.format("%sd %sh %sm", up_d, up_h, up_m)
elseif up_h > 0 then
   upstring = string.format("%sh %sm", up_h, up_m)
else
   upstring = string.format("%sm", up_m)
end

local output = {
   { "model",  model },
   { "distro", os_rel['PRETTY_NAME'] },
   { "kernel", kernel },
   { "shell",  shell },
   { "uptime", upstring },
   { "bat",    batteries },
   { "wifi",   wifi_string },
}

for i, value in pairs(output) do
   local color = ""
   if i % 2 == 1 then
      color = "\27[34m"
   else
      color = "\27[36m"
   end

   print(string.format("%s%6s \27[37m-\27[37m %s", color, value[1], value[2]))
end
print()
