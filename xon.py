#!/bin/python3
"""
AGPLv3
Created by smolsheep
Please don't flood the API.
"""

import json
import http.client
from urllib import request
from rich.console import Console
from rich.table import Table

# Link to get json from
XON_API = "https://xonotic.lifeisabug.com/endpoint/json"

# Create table
console = Console()
serv_list = Table(show_header=True, expand=True, header_style="bold blue")
serv_list.add_column("", justify="right", max_width=6)
serv_list.add_column("Server", justify="left", ratio=0.1, no_wrap=True)
serv_list.add_column("Mode", justify="center", max_width=6)
serv_list.add_column("", justify="center", ratio=0.05)
serv_list.add_column("", justify="right", max_width=6)
serv_list.add_column("IP", justify="left", min_width=22)

# Request json from xon_api
try:
    response = request.urlopen(XON_API)
    rawdata = response.read()
except http.client.IncompleteRead as r:
    # This seems to always trigger.
    rawdata = r.partial

# Restructure locally before sorting
data = json.loads(rawdata)
unsorted = {}
for rserv in data["server"]:
    srv = data["server"][rserv]
    if 0 < srv["numplayers"]:
        unsorted[srv["name"]] = {
            "num": srv["numplayers"],
            "max": srv["maxplayers"],
            "name": srv["realname"],
            "location": srv["geo"],
            "modes": {0: srv["mode"], 1: srv["mode2"]},
            "map": srv["map"],
            "ip": srv["address"],
        }

# Sort by number of players
sorted_servers = dict(
    sorted(unsorted.items(), key=lambda pcount: pcount[1]["num"], reverse=True)
)

# Fill out table and do operations to colorize the data
for _, server in sorted_servers.items():
    # Colorize playercount
    if server["max"] == server["num"]:
        COUNT_COLOR = "bold bright_red"
    if (server["max"] / 3) < server["num"]:
        COUNT_COLOR = "bold green"
    else:
        COUNT_COLOR = "text"
    # Colorize modes
    if "DUEL" == server["modes"][0]:
        MODE_COLOR = "bright_red"
    elif "CA" == server["modes"][0]:
        MODE_COLOR = "bright_cyan"
    else:
        MODE_COLOR = "dark_white"

    server["playercount"] = "[{0}]{1}[/{0}]".format(COUNT_COLOR, server["num"])

    server["formatted_mode"] = "[bold {0}]{1}[/bold {0}]".format(
        MODE_COLOR, server["modes"][0]
    )

    serv_list.add_row(
        server["location"],
        server["name"],
        server["formatted_mode"],
        server["map"],
        server["playercount"],
        server["ip"],
    )

console.clear()
console.print(serv_list)
