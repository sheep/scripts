#!/bin/bash
dunstify -i "image-x-generic" -h "string:x-dunst-stack:exif-notif" "EXIF Data" "$(exiftool -q -p ~/.scripts/notify-exif.fmt $@)"
