#!/bin/python3

# Wake on LAN via customizable command.
#
# Reads toml from ~/.config/rwol.toml
#
# Sample TOML:
# [macs]
# SysName="aa:bb:cc:dd:ee:ff"
# [launch]
# cmd=['rofi', '-p', 'Wake on LAN', '-dmenu']

import subprocess
import tomli
from wakeonlan import send_magic_packet
from xdg.BaseDirectory import xdg_config_home

config = xdg_config_home + "/rwol.toml"
with open(config, "rb") as f:
    conf = tomli.load(f)

# Set launcher command from rwol config
launcher = conf['launch']['cmd']

# Assemble string to pipe into launcher
pipestr = ""
for names in conf['macs']:
    pipestr = pipestr.strip() + "\n" + names

selected = subprocess.run(
                    launcher,
                    input=bytes(pipestr, 'utf-8'),
                    capture_output=True).stdout.decode("utf-8").strip()
if selected == "":
    exit(1)
else:
    print("Waking {0} ({1})".format(selected, conf['macs'][selected]))
    send_magic_packet(conf['macs'][selected])
