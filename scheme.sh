#!/bin/bash
# A color picker using the "pastel" program in order to generate colors.

# If no input provided (./scheme.sh red), use pastel to pick color.
if [ -z ${1+x} ]; then color=$(pastel pick); else color=$1; fi

# Theme for select.
PS3="> "

# Function to improve cleanliness of code, mostly for the all function.
color_loop () {
    for x in $@
    do
        out=$(pastel rotate $x $color | pastel format hex)
        hex=$(pastel format hex $out)
        name=$(pastel format name $out)
        pastel paint "$hex" "Color: $name ($hex)"
    done
}

# Color scheme and harmony layouts. Easy to adjust and add to, in the case new color theory is developed.
complementary="0 180"
split="0 150 210"
analogous="-30 -15 0 15 30"
triadic="0 120 240"
tetra_rectangle="0 60 180 240"
tetra_square="0 90 180 270"

# Color scheme selection.
echo "What color scheme do you want?"
select scheme in Complementary SplitComplementary Analogous Triadic Tetradic_Rectangle Tetradic_Square All
do
    case $scheme in
        "Complementary")
            echo "Mode: $scheme"
            color_loop $complementary
            break
            ;;
        "SplitComplementary")
            echo "Mode: $scheme"
            color_loop $split
            break
            ;;
        "Analogous")
            echo "Mode: $scheme"
            color_loop $analogous
            break
            ;;
        "Triadic")
            echo "Mode: $scheme"
            color_loop $triadic
            break
            ;;
        "Tetradic_Rectangle")
            echo "Mode: $scheme"
            color_loop $tetra_rectangle
            break
            ;;
        "Tetradic_Square")
            echo "Mode: $scheme"
            color_loop $tetra_square
            break
            ;;
        # This is bad. Whatever, homie.
        "All")
            echo "Mode: Complementary"
            color_loop $complementary
            echo "Mode: SplitComplementary"
            color_loop $split
            echo "Mode: Analogous"
            color_loop $analogous
            echo "Mode: Triadic"
            color_loop $triadic
            echo "Mode: Tetradic_Rectangle"
            color_loop $tetra_rectangle
            echo "Mode: Tetradic_Square"
            color_loop $tetra_square
            break
            ;;
    esac
done

