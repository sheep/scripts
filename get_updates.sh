#!/bin/bash

updates=/tmp/update_list.txt

checkupdates > $updates
notify-send -t 60000 "$(cat $updates | wc -l) packages that require updating..." "$(cat $updates | sed 's/ .*//g')"
rm $updates
