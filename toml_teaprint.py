#!/bin/python3

import argparse
import tomli
import subprocess

from escpos.printer import File

printer = File("/dev/usb/lp0")

# Garbage params
columns = 42
lnbreak = "-" * columns

# Setup args.
parser = argparse.ArgumentParser()
parser.add_argument("file", help="Input files", type=argparse.FileType(mode="rb"))
args = parser.parse_args()


def layout(item):
    return """Year: {year}
Maker: {factory}
Tea: {style}
Category: {category}
""".format(
        **item, lnbreak=lnbreak
    )


# Get fortune
def fort(cols):
    out = subprocess.run(
        ["fortune", "-sn", str(cols)], capture_output=True, universal_newlines=True
    )
    out = str(out.stdout).strip("\n")
    return out


# Fast reset for printer.
def reset():
    printer.set(
        align="left",
        font=1,
        bold=0,
        normal_textsize=1,
        height=1,
        width=1,
        density=1,
    )
    # printer.charcode("multilingual")


# Process TOML into dictionary
toml = tomli.load(args.file)

for x in toml:
    tea = {
        "year": str(toml[x]["year"]),
        "factory": toml[x]["factory"],
        "style": toml[x]["style"],
        "category": toml[x]["category"],
    }
    fortune = fort(columns)
    line = layout(tea)

    # Start printing
    printer.set(
        align="left",
        font=0,
        bold=0,
        height=1,
        width=2,
        density=3,
    )
    printer.text(line)
    printer.set(
        align="center",
        font=1,
        bold=0,
        height=1,
        width=1,
        density=3,
    )
    printer.text(lnbreak + "\n")
    printer.text(fortune + "\n")
    printer.text(lnbreak + "\n")
    printer.cut()
    reset()
